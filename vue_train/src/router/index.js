import Vue from 'vue'
import Router from 'vue-router'
import MainIndex from '@/components/MainIndex'
import AboutIndex from "@/components/AboutIndex";
import ContactIndex from "@/components/ContactIndex";
import SigninIndex from "@/components/SigninIndex";
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: "/",
      name: "MainIndex",
      component: MainIndex
    },
    {
      path: "/about",
      name: "AboutIndex",
      component: AboutIndex
    },
    {
      path: "/contact",
      name: "ContactIndex",
      component: ContactIndex
    },
    {
      path: "/sign-in",
      name: "SigninIndex",
      component: SigninIndex
    }
    // {
    //   path: "/NavbarIndex",
    //   name: "HelloWorld2",
    //   component: NavbarIndex
    // }
  ]
});
