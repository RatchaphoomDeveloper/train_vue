var express = require("express");
var router = express.Router();

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", { title: "Express" });
});

router.post("/api/test", function (req, res, next) {
  let reqfilter = req.body
  return res.status(200).json({ message: req.body });
});

module.exports = router;
